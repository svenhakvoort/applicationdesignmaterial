package nl.bioinf.junit_exercise;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimerFilterTest {

    @Test
    void isOK() {

        Primer primer = new Primer();

        HomopolymerFilter filter = new HomopolymerFilter();

        boolean result = filter.isOK(primer);
        assertEquals(true, result, "The given primer is valid");
    }

    @Test
    void isOKRainy() {
        HomopolymerFilter filter = new HomopolymerFilter();

        boolean result = filter.isOK(null);
        assertEquals(false, result, "The given primer is not valid");
    }

    @Test
    void getName() {

    }
}