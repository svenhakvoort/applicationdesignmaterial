package nl.bioinf.junit_exercise;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class PrimerTest {

    @ParameterizedTest(name="{index} => seq={0}, expected={1}")
    @CsvSource({"ACGACCAGAT, 50.00",
            "ACTAGGATACGATCA, 40.00",
            "ACAGCGATCAGCATCAG, 52.941176",
            "ACGCATGCAGCTATAC, 50.00",
            "AHGCHACGJAH, 36.363636"})
    void getGcPercentage(String seq, double expected) {
        Primer primer = new Primer(seq);
        double result = primer.getGcPercentage();
        assertEquals(result, expected/100, 0.001, "The output did not match the expected value");
    }
}