package nl.bioinf.fp_demos;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import nl.bioinf.fp_exercise.User;
import nl.bioinf.fp_exercise.Address;
/**
 * Creation date: 31-1-2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
public class FIdemo {

    public static final List<User> USERS;

    static {
        USERS = new ArrayList<>();
        Address aOne = new Address();
        aOne.setNumber(1);
        aOne.setStreet("Barker Street");
        aOne.setZipCode("7765VV");

        Address aTwo = new Address();
        aTwo.setNumber(32);
        aTwo.setStreet("Overhell");
        aTwo.setZipCode("6667AS");

        User u;
        u = new User();
        u.setAddress(aOne);
        u.setName("Frodo Baggins");
        u.setNumberOfLogins(4);
        USERS.add(u);

        u = new User();
        u.setAddress(aTwo);
        u.setName("Gandalf the Grey");
        u.setNumberOfLogins(5);
        USERS.add(u);

        u = new User();
        u.setAddress(aOne);
        u.setName("Samwise Gamgee");
        u.setNumberOfLogins(1);
        USERS.add(u);

        u = new User();
        u.setAddress(aTwo);
        u.setName("Saruman the White");
        u.setNumberOfLogins(476);
        USERS.add(u);

        u = new User();
        u.setName("Aragorn");
        u.setNumberOfLogins(2);
        USERS.add(u);
    }

    public static void main(String[] args) {
//        NumberCombiner nc = (i, j) -> i*j;
//        System.out.println("2 * 3 = " + nc.combine(2,3));
//        nc = (i, j) -> (int)Math.pow(i, j);
//        System.out.println("2 ^ 3 = " + nc.combine(2,3));
//        combineAndPrint(3, 4, (i, j) -> (int)(i * j * Math.random()));


        //Demo Predicate interface

        filterUsers((user) -> user.getAddress() != null);
        filterUsers((user) -> user.getNumberOfLogins() > 10);

    }


    public static void combineAndPrint(int i, int j, NumberCombiner combiner) {
        System.out.println("combining i = " + i + " and j = " + j + " to " + combiner.combine(i, j));
    }

    /**
     * demo of Predicate Functional Interface
     * @param p
     */
    public static void filterUsers(Predicate<User> p) {
        USERS.stream().filter(p).forEach(user -> System.out.println(user.getName() + " passed"));
    }


}
