package nl.bioinf.multithreading;

import java.util.stream.Stream;

public class SimpleMultithreadingDemo {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            SimpleWorker worker = new SimpleWorker("_" + i + "_");
            Thread t = new Thread(worker);
            t.start();
        }
    }
}
