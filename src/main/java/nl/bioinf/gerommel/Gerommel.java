package nl.bioinf.gerommel;

import com.sun.deploy.util.StringUtils;
import nl.bioinf.fp_demos.FIdemo;
import nl.bioinf.fp_exercise.User;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.join;

public class Gerommel {

    public static void main(String[] args) {
        String input = "De kat krabt de krullen van de trap";

        String singleInput = "krullen";

        Function<String, Word> wordToWord = (s) -> new Word(s).reverse();
//
//        System.out.println(wordToWord.apply(singleInput));
//
        Supplier<Integer> randomNumber = () -> new Random().nextInt(5);
//
//        System.out.println(randomNumber.get());
//
        BiFunction<Integer, String, Word> combineStringAndWord = (i, s) -> new Word(i+" "+s);
//
//        System.out.println(combineStringAndWord.apply(3, singleInput));

//        Arrays.stream(input.split(" "))
//                .filter((w) -> w.length() > 2)
//                .map((w) -> new Word(randomNumber.get()+" "+w))
//                .map(Word::reverse)
//                .forEach(System.out::println);

        // LES 05-03
        input = "De kat krabt de krullen van de trap";
        Map<Integer, List<String>> wordLengthMap = Arrays.stream(input.split(" "))
                .collect(Collectors.groupingBy(String::length));

//        wordLengthMap.forEach((x, y) -> System.out.println(x+" : "+y.stream().collect(Collectors.joining("; "))));

        Stream.of(1,2,3,4,42)
                .map(Gerommel::getUser).map(o -> o.orElse(User.DUMMY)).forEach(x -> System.out.println(x.getName()));

    }

    private static Optional<User> getUser(int id) {
        if (id == 42) {
            return Optional.of(FIdemo.USERS.get(0));
        } else {
            return Optional.empty();
        }
    }

    private static class Word {

        private final String word;

        public Word(String theWord) {
            this.word = theWord;
        }

        public Word reverse() {
            return new Word(new StringBuilder(word).reverse().toString());
        }

        @Override
        public String toString() {
            return word;
        }
    }
}
